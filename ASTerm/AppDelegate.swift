//
//  AppDelegate.swift
//  ASTerm
//
//  Created by Artem Kuchyn on 11/5/16.
//  Copyright © 2016 Artem Kuchyn. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSOutlineViewDelegate {

    @IBOutlet weak var tabWin: NSSegmentedControl!
    @IBOutlet weak var outline: NSOutlineView!
    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var tree: NSTreeController!
    @IBOutlet weak var nodesList: NSArrayController!
    @IBOutlet weak var searchField: NSSearchField!
    @IBOutlet weak var password: NSTextField!
    @IBOutlet weak var sftp: NSTextField!
    @IBOutlet weak var term: NSComboBox!
    @IBOutlet weak var menuBuilder: MenuBuilder!

    let launcher = Launcher()
    let configurations = Configurations()

    override init() {
        super.init()
        ValueTransformer.setValueTransformer(ToTreeTransformer(), forName: NSValueTransformerName("ToTreeTransformer"))
    }

    @IBAction func copySftpToCp(_ sender: Any) {
        copyTextFieldToCP(sftp)
    }

    @IBAction func copyToClipboard(_ sender: NSButton) {
        copyTextFieldToCP(password)
    }

    func copyTextFieldToCP(_ field: NSTextField) {
        if (!field.stringValue.isEmpty) {
            let pasteboard = NSPasteboard.general
            pasteboard.declareTypes([NSPasteboard.PasteboardType.string], owner: nil)
            pasteboard.setString(field.stringValue, forType: NSPasteboard.PasteboardType.string)
        }
    }

    @IBAction func openFile(_ sender: NSButton) {
        loadFile(param: "session_xml_files")
    }

    func loadFile(param: String) {
        let dialog = NSOpenPanel();

        dialog.title = "Choose a .xml file";
        dialog.showsResizeIndicator = true;
        dialog.showsHiddenFiles = false;
        dialog.canChooseDirectories = true;
        dialog.canCreateDirectories = true;
        dialog.allowsMultipleSelection = false;
        dialog.allowedFileTypes = ["xml"];

        if (dialog.runModal() == NSApplication.ModalResponse.OK) {
            let result = dialog.url

            if (result != nil) {
                let path = "file://" + (result?.path)!;
                nodesList.content = readFiles(pathes: configurations.sessions.appendFile(path))
            }
        } else {
            return
        }
    }

    func outlineView(_ outlineView: NSOutlineView, willDisplayOutlineCell cell: Any, for tableColumn: NSTableColumn?, item: Any) {
        outline.expandItem(item, expandChildren: true)

    }

    @IBAction func onSearch(_ sender: Any) {
        outline.expandItem(nil, expandChildren: true)
    }


    func outlineView(_ outlineView: NSOutlineView, willDisplayCell cell: Any, for tableColumn: NSTableColumn?, item: Any) {
        outline.expandItem(item, expandChildren: true)
    }


    @IBAction func expand(_ sender: Any) {
        outline.expandItem(nil, expandChildren: true)
    }

    @IBAction func collapse(_ sender: Any) {
        outline.collapseItem(nil, collapseChildren: true)
    }

    @IBAction func openInTerminal(_ sender: Any) {
        open(f: { node in
            switch tabWin.selectedSegment {
            case 0: launcher.openInTerminalTab(node: node)
            default:
                switch term.stringValue {
                case "iTerm2":  launcher.openInITerm(node: node)
                default: launcher.openInTerminal(node: node)
                }
            }
        })
    }

    @IBAction func openHmc(_ sender: Any) {
        open(f: { node in
            launcher.openInBrowser(node: node, path: "/hmc/hybris")
        })
    }

    @IBAction func openHac(_ sender: Any) {
        open(f: { node in
            launcher.openInBrowser(node: node, path: "/hac")
        })
    }

    @IBAction func openBackoffice(_ sender: Any) {
        open(f: { node in
            launcher.openInBrowser(node: node, path: "/backoffice")
        })
    }

    @IBAction func openBrowser(_ sender: Any) {
        open(f: { node in
            launcher.openInBrowser(node: node, path: "/")
        })
    }

    @IBAction func openSftp(_ sender: Any) {
        open(f: { node in
            launcher.openInFilezilla(node: node)
        })
    }

    func open(f: (Node) -> ()) {
        if (!tree.selectedObjects.isEmpty) {
            if let node = tree.selectedObjects[0] as? Node {
                f(node)
            }
        }
    }

    func readFiles(pathes: [String]) -> [SPItem] {
        let items = pathes.flatMap { path -> [SPItem] in
            XMLHelper().load(file: URL(string: path)!)
        }

        return items
    }

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        tabWin.selectedSegment = configurations.getTerminal()
        term.selectItem(withObjectValue: configurations.getApplication())
        nodesList.content = readFiles(pathes: configurations.sessions.getFiles())

//        menuBuilder.build()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        configurations.setTerminal(tabWin.selectedSegment)
        configurations.setApplication(term.stringValue)

        // Insert code here to tear down your application
    }

    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }

}

extension Array where Element: SPItem {

    func removingDuplicates() -> Array {
        return reduce(into: []) { result, element in
            if !result.contains(element) {
                result.append(element)
            }
        }
    }

    func duplicates() -> Array {
        return []
    }
}

