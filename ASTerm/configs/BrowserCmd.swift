//
//  BrowserCmd.swift
//  ASTerm
//
//  Created by Artem Kuchyn on 5/1/18.
//  Copyright © 2018 Artem Kuchyn. All rights reserved.
//

import Cocoa

@objcMembers
class BrowserCmd: NSObject {
    var template: String
    
    init(template: String) {
        self.template = template
    }
    
    func open(host: String) {
        let url = template.replacingOccurrences(of: "{host}", with: host)
        NSWorkspace.shared.open(URL(string: url)!)
    }
}
