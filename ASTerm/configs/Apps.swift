//
//  Apps.swift
//  ASTerm
//
//  Created by Artem Kuchyn on 5/1/18.
//  Copyright © 2018 Artem Kuchyn. All rights reserved.
//

import Cocoa

enum Apps {
    case terminal
    case iterm2
}
