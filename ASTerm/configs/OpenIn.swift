//
//  OpenIn.swift
//  ASTerm
//
//  Created by Artem Kuchyn on 5/1/18.
//  Copyright © 2018 Artem Kuchyn. All rights reserved.
//

import Cocoa

enum OpenIn: Int {
    case window = 1
    case tab = 0
}
