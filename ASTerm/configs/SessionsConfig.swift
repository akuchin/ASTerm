//
//  SessionsConfig.swift
//  ASTerm
//
//  Created by Artem Kuchyn on 5/1/18.
//  Copyright © 2018 Artem Kuchyn. All rights reserved.
//

import Cocoa

class SessionsConfig: NSObject {
    
    func setFiles(_ value: [String]) {
        UserDefaults.standard.setValue(value, forKey: "session_xml_files")
    }
    
    func appendFile(_ value: String) -> [String]  {
        var files = getFiles()
        files.append(value)
        setFiles(files)
        return files
    }
    
    func reset() {
        setFiles([])
    }
    
    func getFiles() -> [String] {
        let prop = UserDefaults.standard.stringArray(forKey: "session_xml_files")
        return prop ?? []
    }

}
