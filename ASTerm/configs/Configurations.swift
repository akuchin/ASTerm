//
//  Configurations.swift
//  ASTerm
//
//  Created by Artem Kuchyn on 5/1/18.
//  Copyright © 2018 Artem Kuchyn. All rights reserved.
//

import Cocoa

class Configurations: NSObject {
    
    let sessions = SessionsConfig()
    
    func setApplication(_ value: String) {
        UserDefaults.standard.setValue(value, forKey: "open_in_term")
    }

    func setTerminal(_ value: Int) {
        UserDefaults.standard.setValue(value, forKey: "open_in_tab")
    }
    
    func getApplication() -> String {
        let prop = UserDefaults.standard.string(forKey: "open_in_term")
        return prop ?? "Terminal"
    }
    
    func getTerminal() -> Int {
        return UserDefaults.standard.integer(forKey: "open_in_tab")
    }
    
}
