//
//  MenuBuilder.swift
//  ASTerm
//
//  Created by Artem Kuchyn on 5/1/18.
//  Copyright © 2018 Artem Kuchyn. All rights reserved.
//

import Cocoa

@objcMembers
class MenuBuilder: NSObject {
    
    @IBOutlet weak var popupMenu: NSMenu!
    @IBOutlet weak var tree: NSTreeController!
    
    func build() {
        let aboutMenuItem = NSMenuItem()
        aboutMenuItem.title = "About"
        aboutMenuItem.target = self
        aboutMenuItem.action =  #selector(openin(_:))

        popupMenu.addItem(aboutMenuItem)

        print("aboutMenuItem: \(aboutMenuItem.isEnabled) \(aboutMenuItem.isHighlighted)")

        let a2 = NSMenuItem()
        a2.title = "AAAA"
        a2.target = self
        a2.action =  #selector(openin(_:))
        a2.isEnabled = true
        popupMenu.addItem(a2)

    }

    func openin2(_ template: String) {
        print("s\(template)")
        if let node = current() {
            print(node.host)
        }
    }

    func openin(_ sender: NSMenuItem) {
        print("template \(sender.title)")
        if let node = current() {
            print(node.host)
        }
    }
    
    func current() -> Node? {
        if (!tree.selectedObjects.isEmpty) {
            if let node = tree.selectedObjects[0] as? Node {
                return node
            }
        }
        return nil
    }
    
    

}
