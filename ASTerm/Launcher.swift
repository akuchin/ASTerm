//
//  Launcher.swift
//  suptut
//
//  Created by Artem Kuchyn on 11/1/16.
//  Copyright © 2016 Artem Kuchyn. All rights reserved.
//

import Cocoa

class Launcher: NSObject {

    func openInITerm(node: Node) {
        let cmd = """
        tell application "iTerm"
        tell current window
        create tab with default profile
        tell current session
        write text "sshpass -p '\(node.password)' ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null \(node.username)@\(node.host)"
        end tell
        end tell
        end tell
        """
        
        let task = Process()
        task.launchPath = "/usr/bin/osascript"
        task.arguments = ["-e", cmd]
        task.launch()
    }
    
    func openInTerminal(node: Node) {
        let task = Process()
        task.launchPath = "/usr/bin/osascript"
        task.arguments = ["-e", "tell app \"Terminal\" to do script \" sshpass -p '\(node.password)' ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null \(node.username)@\(node.host)\""]
        task.launch()
    }
    
    func openInTerminalTab(node: Node) {
        let task = Process()
        task.launchPath = "/usr/bin/osascript"
        task.arguments = ["-e", "tell application \"Terminal\" to activate",
                          "-e", "tell application \"System Events\" to tell process \"Terminal\" to keystroke \"t\" using command down",
                          "-e", "tell app \"Terminal\" to do script \" sshpass -p '\(node.password)' ssh -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null \(node.username)@\(node.host)\" in selected tab of the front window"]
        task.launch()
    }
    
    func openInFilezilla(node: Node) {
        let task = Process()
        task.launchPath = "/usr/bin/open"
        task.arguments = ["-a", "Filezilla", "--new", "--args", node.sftp]
        task.launch()
    }
    
    
    func openInBrowser(node: Node, path: String) {
        let url = "http://\(node.host):9001\(path)"
        NSWorkspace.shared.open(URL(string: url)!)
    }
    

}
