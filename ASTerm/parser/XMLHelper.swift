//
//  XMLHelper.swift
//  suptut
//
//  Created by Artem Kuchyn on 11/1/16.
//  Copyright © 2016 Artem Kuchyn. All rights reserved.
//

import Cocoa

class XMLHelper: NSObject, XMLParserDelegate {
  
    var xmlParser: XMLParser!
    var parsedList: [SPItem] = []

    func load(file: URL)-> [SPItem] {
        parsedList = [];
        self.xmlParser = XMLParser(contentsOf: file)
        self.xmlParser.delegate = self
        self.xmlParser.parse()
        
        return parsedList;
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String: String]) {
        
        if elementName == "SessionData" {
            parsedList.append(SPItem(sessionId: attributeDict["SessionId"]!,
                                     sessionName: attributeDict["SessionName"]!,
                                     host: attributeDict["Host"]!,
                                     port: attributeDict["Port"]!,
                                     proto: attributeDict["Proto"]!,
                                     puttySession: attributeDict["PuttySession"]!,
                                     username: attributeDict["Username"]!,
                                     extraArgs: attributeDict["ExtraArgs"]!))
        }
    }
    
}
