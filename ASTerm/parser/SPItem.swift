//
//  SPItem.swift
//  suptut
//
//  Created by Artem Kuchyn on 10/30/16.
//  Copyright © 2016 Artem Kuchyn. All rights reserved.
//

import Cocoa

@objcMembers
class SPItem: NSObject {
    var sessionId: String
    var sessionName: String
    var host: String
    var port: String
    var proto: String
    var puttySession: String
    var username: String
    var extraArgs: String

    init(sessionId: String,
        sessionName: String,
        host: String,
        port: String,
        proto: String,
        puttySession: String,
        username: String,
        extraArgs: String) {

        self.sessionId=sessionId
        self.sessionName=sessionName
        self.host=host
        self.port=port
        self.proto=proto
        self.puttySession=puttySession
        self.username=username
        self.extraArgs=extraArgs

    }

    static func ==(lhs: SPItem, rhs: SPItem) -> Bool {
        return lhs.sessionId == rhs.sessionId
    }

}
