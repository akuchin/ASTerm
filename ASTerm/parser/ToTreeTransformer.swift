//
//  ToTreeTransformer.swift
//  ASTerm
//
//  Created by Artem Kuchyn on 11/19/16.
//  Copyright © 2016 Artem Kuchyn. All rights reserved.
//

import Cocoa

class ToTreeTransformer: ValueTransformer {

    let nodeParser = NodeParser();

    override func transformedValue(_ value: Any?) -> Any? {
        if let items = value as? [SPItem] {
            return nodeParser.load(items: items)
        } else {
            return []
        }
    }

}
