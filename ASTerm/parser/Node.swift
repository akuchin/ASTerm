//
//  Node.swift
//  tttt
//
//  Created by Artem Kuchyn on 11/5/16.
//  Copyright © 2016 Artem Kuchyn. All rights reserved.
//

import Cocoa

@objcMembers
class Node: NSObject {
    var path: String
    var name: String
    var host: String
    var sftp: String
    var username: String
    var password: String
    var nodeType: String
    var children: [Node]
    
    override func isEqual(_ object: Any?) -> Bool {
        if let that = object as? Node {
            return path == that.path
        } else {
            return false
        }
    }
    
    override var hash: Int {
        return path.hashValue
    }
    
    
    init(name: String,
         host: String,
         username: String,
         password: String,
         nodeType: String,
         children: [Node],
         path: String){
        self.name = name;
        self.children = children
        self.username=username
        self.password=password
        self.host=host
        self.path=path
        self.nodeType = nodeType
        self.sftp = host.isEmpty ? "": "sftp://\(username):\(password)@\(host)"
        
    }
    
    convenience init(name: String, nodeType: String, path: String){
        self.init(name: name, host: "",username: "",password: "", nodeType:nodeType, children: [], path: path)
    }
    convenience override init(){
        self.init(name: "default", host: "",username: "",password: "", nodeType:"",  children: [], path: "/")
    }
    
    func addNode(item: SPItem, path: [String]) {
        let exist = children.first { (n) -> Bool in
            return n.name == path[0]
        }
        if exist != nil {
            let newPath: [String] = Array(path.suffix(from: 1))
            exist!.addNode(item: item, path: newPath)
        } else {
            if (path.count > 1) {
                self.children.append(Node(name: path[0], nodeType:item.puttySession, path:path[0]+item.sessionId))
                addNode(item:item, path: path)
            } else {
                self.children.append(Node(name: path[0], host: item.host,
                                          username: item.username,
                                          password: item.extraArgs.replacingOccurrences(of: "-pw ", with: ""),
                                          nodeType: item.puttySession, children:[], path: path[0]+item.sessionId))
            }
        }
    }
    
    func image() -> NSImage? {
        if (host.isEmpty) { return nil }
        if (path.contains("DEPRECATED")) { return NSImage(imageLiteralResourceName: "NSStatusNoneFlat") }

        switch nodeType {
        case "PROD":
            return NSImage(imageLiteralResourceName: "NSStatusAway")
        case "UAT":
            return NSImage(imageLiteralResourceName: "NSStatusAvailableFlat")
        case "DEV":
            return NSImage(imageLiteralResourceName: "NSStatusIdle")
        case "PRE":
            return NSImage(imageLiteralResourceName: "NSStatusPartiallyAvailableFlat")
        default:
            return nil
            }
        
    }
    
    func color() -> NSColor {
        if (path.contains("DEPRECATED")) { return NSColor.gray }
        return NSColor.black
    }
    
}
