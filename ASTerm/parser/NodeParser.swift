//
//  NodeParser.swift
//  ASTerm
//
//  Created by Artem Kuchyn on 11/16/16.
//  Copyright © 2016 Artem Kuchyn. All rights reserved.
//

import Cocoa

class NodeParser: NSObject {

    func load(items: [SPItem])-> [Node] {
        let rootNode: Node = Node(name:"root", nodeType: "", path:"root")
        
        for item in items {
            rootNode.addNode(item: item, path: item.sessionId.components(separatedBy: "/"));
        }
        
        return rootNode.children
    }
    
}
